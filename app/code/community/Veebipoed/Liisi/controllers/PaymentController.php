<?php

class Veebipoed_Liisi_PaymentController extends Mage_Core_Controller_Front_Action
{
    public function validationAction()
	{
		$order = Mage::getModel('sales/order')->load(
			Mage::getSingleton('checkout/session')->getLastOrderId()
		);
		if (!$order->getEmailSent()) {
			try {
				$order->sendNewOrderEmail();
				$order->save();
			} catch (Exception $ex) {
				//Only email
			}
		}
		
		$this->loadLayout();
        $this->renderLayout();
    }
	
	public function returnAction()
	{
		$helper = Mage::helper('liisi/data');
		$liisi_api = $helper->getLiisiAPI();
		if ($liisi_api->vertifySign($this->getRequest()->getParams())) {
			$id_order = $this->getRequest()->getParam('VK_STAMP');
			$service = $this->getRequest()->getParam('VK_SERVICE');
			$auto = $this->getRequest()->getParam('VK_AUTO');
			$order = Mage::getModel('sales/order')->loadByIncrementId((int)$id_order);

			switch ($service) {
				case Veebipoed_Liisi_Model_Liisi::RESPONSE_1111: //Success
					if ($order->canInvoice())
					{
						$invoice = $order->prepareInvoice();
						$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
						$invoice->register();
						$invoice->save();
						$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
						$order->addStatusHistoryComment('Successful Liisi payment!');
						$order->save();
						try {
							$invoice->sendEmail(true, '');
						} catch (Exception $ex) {
							//Only email
						}
					}
					$this->_redirect('checkout/onepage/success');
					break;
				case Veebipoed_Liisi_Model_Liisi::RESPONSE_1911: //Cancel
					$order->cancel();
					$order->addStatusHistoryComment('Order cancelled by Liisi');
					$order->save();
					$this->_redirect('checkout/onepage/failure');
					break;
				default: //Error
					$this->_redirect('checkout/onepage/failure');
					break;
			}
				
		} else {
			$this->_redirect('/');
		}
    }
}
