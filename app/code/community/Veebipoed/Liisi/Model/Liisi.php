<?php

class Veebipoed_Liisi_Model_Liisi
{
    const TEST_URL = 'https://prelive.liisi.ee/api/ipizza/';
    const LIVE_URL = 'https://klient.liisi.ee/api/ipizza/';

    const TEST = 0;
    const LIVE = 1;

    const SERVICE_1011 = 1011;
    const SERVICE_1012 = 1012;

    const RESPONSE_1111 = 1111;
    const RESPONSE_1911 = 1911;

    // Default values
    protected $charset = 'UTF-8';
    protected $language = 'EST';
    protected $service = '1012';
    protected $version = '008';
    protected $name = '';
    protected $account = '';

    // user-defined variables
    protected $snd;
    protected $private_key;
    protected $public_key;
    protected $private_key_pass = false;
    protected $mode;

    protected $url;

    /**
     * Magento factory pattern awesomeness
     */
    public function __construct(array $parameters)
    {
        $keys = array('mode', 'snd', 'private_key', 'public_key');
		foreach ($keys as $key) {
            if (!isset($parameters[$key])) {
                throw new Exception('Blame magento factory pattern');
            }

            $this->{$key} = $parameters[$key];
        }

        if (!empty($parameters['private_key_pass'])) {
            $this->private_key_pass = $parameters['private_key_pass'];
        }

        $this->setUrl();
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    protected function setUrl()
    {
        if ($this->mode == self::LIVE) {
            $this->url = self::LIVE_URL;
        } else {
            $this->url = self::TEST_URL;
        }
    }

    protected function padding($string)
    {
        if (!empty($this->charset)) {
            return str_pad(mb_strlen($string, $this->charset), 3, '0', STR_PAD_LEFT);
        } else {
            return str_pad(strlen($string), 3, '0', STR_PAD_LEFT);
        }
    }

    public function getFormFields(Veebipoed_Liisi_Model_Order $order, $return_url)
    {
        $form_values = array();
        $form_values['VK_SERVICE'] = $this->service;
        $form_values['VK_VERSION'] = $this->version;
        $form_values['VK_SND_ID'] = $this->snd;
        $form_values['VK_STAMP'] = $order->getNumber();
        $form_values['VK_AMOUNT'] = $order->getAmount();
        $form_values['VK_CURR'] = $order->getCurrency();

        if ($this->service == self::SERVICE_1011) {
            $form_values['VK_ACC'] = $this->account;
            $form_values['VK_NAME'] = $this->name;
        }
        $form_values['VK_REF'] = $order->getReference();
        $form_values['VK_MSG'] = $order->getMessage();
        $form_values['VK_RETURN'] = rtrim($return_url, '/');
        $form_values['VK_CANCEL'] = rtrim($return_url, '/');

		$datetime = new DateTime();
        $form_values['VK_DATETIME'] = $datetime->format(DateTime::ISO8601);
        //$form_values['VK_DATETIME'] = Mage::getModel('core/date')->date(DATE_ISO8601);

        $form_values['VK_MAC'] = $this->generateMAC($form_values);
        $form_values['VK_ENCODING'] = $this->charset;
        $form_values['VK_LANG'] = $this->language;

        return $form_values;
    }

    // Wrong place ???
    public function getUrl()
    {
        return $this->url;
    }

    protected function generateMAC($values)
    {
        $data = array();
        foreach ($values as $value) {
            $data[] = $this->padding($value) . $value;
        }

        $private = openssl_get_privatekey(
            $this->private_key,
            $this->private_key_pass
        );
		
		if (!$private) {
            throw new Exception('Invalid liisi private key');
        }

        openssl_sign(implode('', $data), $signature, $private);
		$mac = base64_encode($signature);
        openssl_free_key($private);
		
		return $mac;
    }

    protected function extractRequestData(array $request)
    {
        $data = array();
        $fields = array(
            'VK_SERVICE',
            'VK_VERSION',
            'VK_SND_ID',
            'VK_REC_ID',
            'VK_STAMP',
        );

        $fields_1111 = array(
            'VK_T_NO',
            'VK_AMOUNT',
            'VK_CURR',
            'VK_REC_ACC',
            'VK_REC_NAME',
            'VK_SND_ACC',
            'VK_SND_NAME',
            'VK_REF',
            'VK_MSG',
            'VK_T_DATETIME',
        );
        $fields_1911 = array('VK_REF', 'VK_MSG');

        if (isset($request['VK_SERVICE']) &&
            in_array($request['VK_SERVICE'], array(self::RESPONSE_1911, self::RESPONSE_1111))
        ) {
            $fields = array_merge($fields, ${'fields_'.$request['VK_SERVICE']});
        } else {
            throw new Exception('Invalid service type');
        }

        foreach ($fields as $field) {
            if (isset($request[$field])) {
                $data[$field] = $this->padding($request[$field]).$request[$field];
            } else {
                throw new Exception(sprintf('Missing %s field', $field));
            }
        }

        return $data;
    }

    protected function extraRequestSign(array $request)
    {
        if (isset($request['VK_MAC'])) {
            return $request['VK_MAC'];
        } else {
            throw new Exception('Missing signature');
        }
    }

    public function vertifySign(array $request)
    {
        try {
            $signature = $this->extraRequestSign($request);
            $data = $this->extractRequestData($request);
        } catch (Exception $e) {
            return false;
        }

        $signature = base64_decode($signature);
        $public_key = openssl_get_publickey($this->public_key);
        $out = openssl_verify(implode('', $data), $signature, $public_key);
        openssl_free_key($public_key);

        return $out;
    }
}
