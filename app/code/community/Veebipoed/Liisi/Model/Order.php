<?php

class Veebipoed_Liisi_Model_Order
{
    protected $number;
    protected $amount;

    protected $currency = 'EUR';
    protected $reference = '';
    protected $message;

    public function __construct(array $parameters)
    {
        $keys = array('number', 'amount');
        foreach ($keys as $key) {
            if (!isset($parameters[$key])) {
                throw new Exception('Blame magento factory pattern');
            }

            $this->{$key} = $parameters[$key];
        }
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getAmount()
    {
        return number_format((float)$this->amount, 2, '.', '');
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
