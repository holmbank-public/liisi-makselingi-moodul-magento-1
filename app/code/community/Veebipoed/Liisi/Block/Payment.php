<?php

class Veebipoed_Liisi_Block_Payment extends Mage_Payment_Block_Form
{
    public function getForm()
    {
        $helper = Mage::helper('liisi/data');

        $order_id = Mage::getSingleton('checkout/session')->getLastOrderId();
        $order = Mage::getModel('sales/order')->load($order_id);

        $liisi_order = $helper->buildLiisiOrder($order);
        $liisi_api = $helper->getLiisiAPI();
		
		return $liisi_api->getFormFields(
            $liisi_order,
            Mage::getUrl('liisi/payment/return', array('_secure' => true, '_nosid' => true))
        );
    }

    public function getLiisiUrl()
    {
        $helper = Mage::helper('liisi/data');
        $liisi_api = $helper->getLiisiAPI();
        return $liisi_api->getUrl();
    }
}
