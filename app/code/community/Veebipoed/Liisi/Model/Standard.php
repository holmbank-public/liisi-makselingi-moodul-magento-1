<?php

class Veebipoed_Liisi_Model_Standard extends Mage_Payment_Model_Method_Abstract
{
	protected $_code = 'liisi';

	protected $_isInitializeNeeded      = true;
	protected $_canUseInternal          = true;
	protected $_canUseForMultishipping  = false;

	public function getOrderPlaceRedirectUrl()
    {
		return Mage::getUrl('liisi/payment/validation', array('_secure' => true));
	}
}
