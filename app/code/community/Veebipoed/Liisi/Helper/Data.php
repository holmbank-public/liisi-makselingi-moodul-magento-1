<?php

class Veebipoed_Liisi_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getLiisiAPI()
    {
        $prefix = 'payment/liisi/';
        $parameters = array(
            'mode' => !Mage::getStoreConfig($prefix.'test_mode'),
            'snd' => Mage::getStoreConfig($prefix.'snd_id'),
            'private_key' => Mage::getStoreConfig($prefix.'private_key'),
            'public_key' => Mage::getStoreConfig($prefix.'public_key'),
            'private_key_pass' => Mage::getStoreConfig($prefix.'private_key_pass'),
        );
        $liisi = Mage::getSingleton('liisi/liisi', $parameters);
        $liisi->setLanguage($this->getMagentoLanguage());

        return $liisi;
    }

    public function buildLiisiOrder(Mage_Sales_Model_Order $order)
    {
        $parameters = array(
            'number' => substr($order->getIncrementId(), 0, 20),
            'amount' => $order->getTotalDue(),
        );

        $liisi_order = Mage::getModel('liisi/order', $parameters);
        $liisi_order->setCurrency($order->getOrderCurrencyCode());
		$liisi_order->setMessage(__('Order number').' '.$liisi_order->getNumber());

		return $liisi_order;
    }

    public function getMagentoLanguage()
    {
        switch (Mage::app()->getLocale()->getLocaleCode()) {
            case 'et_EE':
                $language = 'EST';
                break;
            case 'lt_LT':
                $language = 'LIT';
                break;
            case 'lv_LV':
                $language = 'LAT';
                break;
            case 'ru_RU':
                $language = 'RUS';
                break;
            default:
                $language = 'ENG';
                break;
        }

        return $language;
    }
}
